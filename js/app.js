var myApp = angular.module('myApp', []);

// controller to import data from json file
myApp.controller('MyController', function MyController($scope,$http) {
  $http.get('js/lenders.json').then(function(response) {
    $scope.lenders = response.data;
  });
});

//directive for loading spinner
myApp.directive('loading',   ['$http' ,function ($http) {
     return {
         restrict: 'A',
         template: '<div class="fa fa-spinner fa-spin fa-4x" /> </div>',
         link: function (scope, elm, attrs)
         {
             scope.isLoading = function () {
                 return $http.pendingRequests.length > 0;
             };

             scope.$watch(scope.isLoading, function (v)
             {
                 if (v) {
                            elm.css('display', 'block');
                        } else {
                            elm.css('display', 'none');
                        }
             });
         }
     };
 }])

// directive for sticky table header
myApp.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {

        var windowScrollTop = this.pageYOffset;
        var divAnchor = document.getElementById("Header-anchor");
        var divHeader = document.getElementById("Header");

        if (windowScrollTop > divAnchor.offsetTop) {
            divHeader.style.position = "fixed";
            divHeader.style.top = "0px";
        } else {
            divHeader.style.position = "relative";
            divHeader.style.top = "";
        }

        });
    };
});